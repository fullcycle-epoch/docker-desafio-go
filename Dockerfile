# syntax=docker/dockerfile:1
FROM golang:latest as builder

LABEL com.epoch.company="epoch"
LABEL com.epoch.version="1.0.0"
LABEL com.epoch.description="Dockerfile for FullCycle training 3.0"
LABEL com.epoch.course="Docker"
LABEL com.epoch.challenge="Desafio Go"
LABEL com.epoch.date="20230919"

WORKDIR /src
COPY <<EOF ./main.go
package main

import "fmt"

func main() {
str := `
 __________________
< FullCycle Rocks! >
 ------------------
                  \\   ^__^
                   \\  (oo)\\_______
                      (__)\\       )\\/\\
                          ||---ww |
                          ||     ||
`
  fmt.Println(str)
}
EOF
RUN go build -o /bin/fullcycle ./main.go

FROM scratch as runner
COPY --from=builder /bin/fullcycle /usr/bin/local/fullcycle
CMD ["/usr/bin/local/fullcycle"]

